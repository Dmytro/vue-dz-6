import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import About from './views/About.vue'
import NotFound from './views/NotFound.vue'
import SubPage from './views/SubPage.vue'
import Child from './views/Child.vue'

Vue.use(Router)

/*
  1. base example
  2. dynamic routes
  3. nested

*/
export default new Router({
  // mode: 'hash',
  mode: 'history',
  // base: process.env.BASE_URL,
  // base: 'base',
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/about',
      name: 'about',
      component: About,
      children: [
        {path: 'company', component: Child }
      ]
    },
    // {
    //   path: '/about/company',
    //   component: Child
    // },
    {
      path: '/404',
      component: NotFound
    },
    {
      path: '*',
      redirect: '/404'
      // component: NotFound
    }
  ]
})
