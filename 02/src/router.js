import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import About from './views/About.vue'
import NotFound from './views/NotFound.vue'
import SubPage from './views/SubPage.vue'
import Child from './views/Child.vue'

Vue.use(Router)

/*
  1. base example
  2. dynamic routes
  3. nested
  4. router-link

  6. meta and props

*/
export default new Router({
  // mode: 'hash',
  mode: 'history',
  // linkActiveClass: 'text-red',
  // linkExactActiveClass: 'text-orange',
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/about',
      name: 'about',
      component: About,
      children: [
        {path: 'company', component: Child }
      ],
      meta: {
        isOk: true
      }
    },
    {
      path: '/:subpage',
      name: 'subpage',
      component: SubPage,
      // props: true
      // props: {isOk: true}
      props: (route) => {
        return {
          isOk: true,
          subpage: route.params.subpage
        }
      }
    },
    {
      path: '/404',
      component: NotFound
    },
    {
      path: '*',
      redirect: '/404'
      // component: NotFound
    }
  ]
})
