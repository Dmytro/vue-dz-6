# vue-dz-6

Использовать https://jobs.github.com/api через прокси https://github.com/cmccormack/github-jobs-proxy

Сделать "клон" https://jobs.github.com с использованием vue-router

Страницы:
- / главная с формой поиска вакансий (+ блок Hot Searches из урока)
- /positions - (форма поиска присутствует)
- /positions/:id - 1 вакансия (делать запрос по id, GET /positions/ID.json)


получаем 50 вакансий

внизу кнопка делает запрос за след. порцией вакансий,
если вакансий меньше 50, кнопку не выводим

Для запросов используем axios, пример в проекте с jobs
