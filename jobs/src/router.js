import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Faq from './views/Faq.vue'
import Positions from './views/Positions.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/faq',
      name: 'faq',
      component: Faq
    },
    {
      path: '/positions',
      name: 'positions',
      component: Positions
    },
    // {
    //   path: '/positions/:description',
    //   name: 'positions',
    //   component: Positions
    // }
  ]
})
